//
//  LAAppDelegate.m
//  LAPub
//
//  Created by Stéphane Couzinier on 11/04/2015.
//  Copyright (c) 2015 Stéphane Couzinier. All rights reserved.
//

#import "LAAppDelegate.h"
#import "LAPartenaire.h"
@interface LAAppDelegate ()<LAInterstitialDelegate>
@end

@implementation LAAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [self initBlocPartenaire];
    [self displaySplashInterstitialOnViewController:self.window.rootViewController];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    [self initBlocPartenaire];
    [self performSelector:@selector(displaySplashInterstitialOnViewController:) withObject:self.window.rootViewController afterDelay:0.1];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void) initBlocPartenaire{
    LAPartenaire *partenaire=[LAPartenaire alloc];
    [partenaire saveUrl:@"https://ota.ladmedia.fr/partenaire/elle/ios_article.html" toFile:@"ios_article.html" completion:^{
        NSLog(@"Fichier partenaire : %@",[partenaire getContentForFile:@"ios_article.html" withParameter:nil]);
    }];
}


- (void) didDismissInterstitial
{

}

- (void)displaySplashInterstitialOnViewController:(UIViewController*)aViewController
{
    @try
    {
        LAAdTags *splashInterTags;
        
        splashInterTags = [[LAAdTags alloc] initWithTag:@"/12271007/dfp_tablettes_tele7jours_s134543/interstitiel_rg_r167614/f167622_d244_dm24_portrait_interstitiel_tablette_portrait_768x1024"];
        
        if (self.intersAdController != nil)
        {
            self.intersAdController = nil;
        }
        
        LAInterstitial *tmp = [[LAInterstitial alloc] initForSplashAdWithAdtagID:splashInterTags];
        [tmp setDelegate:self];
        [self setIntersAdController:tmp];
        
        [self.intersAdController displaySplashAdOnViewController:aViewController AndWithSplashImg:FALSE];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"\n\n\n!!! AppDelegate : displayInterAd raise an exception=> name : %@ | reason : %@ !!!\n\n\n",[exception name],[exception reason]);
    }
}

@end
