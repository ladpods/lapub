//
//  LAViewController.h
//  LAPub
//
//  Created by Stéphane Couzinier on 11/04/2015.
//  Copyright (c) 2015 Stéphane Couzinier. All rights reserved.
//

@import UIKit;
#import "LABannerView.h"

@interface LAViewController : UIViewController <LABannerViewDelegate>
{
    LABannerView            *bannerView;

}
@property (nonatomic, retain) LABannerView                      *bannerView; //DFP

@end
