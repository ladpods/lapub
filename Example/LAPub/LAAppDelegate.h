//
//  LAAppDelegate.h
//  LAPub
//
//  Created by Stéphane Couzinier on 11/04/2015.
//  Copyright (c) 2015 Stéphane Couzinier. All rights reserved.
//
#import "LADFPAdsHeader.h"

@import UIKit;

@interface LAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (retain, nonatomic)           LAInterstitial                      *intersAdController;

@end
