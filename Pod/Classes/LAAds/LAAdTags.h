//
//  LAAdTags.h
//  LAD-DFP
//
//  Created by DUPUY Yann on 26/12/12.
//  Copyright (c) 2012 DUPUY Yann. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LAAdTags : NSObject
{
    NSString *defaultPortraitTag;
    NSString *defaultLandscapeTag;
}

/********* ATTRIBUTES **********/

@property (nonatomic,strong)    NSString *defaultPortraitTag;
@property (nonatomic,strong)    NSString *defaultLandscapeTag;


/********* METHODS **********/

#pragma mark - Initialization Methods

/// Default Initialization
- (id)initWithTag:(NSString*) aTag;

/// Default initialization with take in account of orientation (Portrait/Landscape)
- (id)initWithPortraitTag:(NSString*) aPortraitTag AndLandscapeTag:(NSString*) aLandscapeTag;


@end
