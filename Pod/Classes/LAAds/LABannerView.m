//
//  LABannerView.m
//  LAD-DFP
//
//  Created by DUPUY Yann on 21/12/12.
//  Copyright (c) 2012 DUPUY Yann. All rights reserved.
//

#import "LABannerView.h"
#import "GeolocManager.h"
#import "MPAdView.h"
//----------------------------------------------
//---------------- DEBUG MACRO -----------------
//----------------------------------------------

#pragma mark - Debug configuration

#ifdef DEBUG
//#define DEBUG_ENABLE //Activate Debug log only in this file
#endif

/**
 *  - Activate Debug Log for all file  if  "DEBUG_VERBOSE" define in GCC PREPROCESSOR MACRO (build setting)
 *  - Activate Debug only in a file if variable DEBUG_ENABLE  define in the file
 */

#if (defined DEBUG_ENABLE || defined DEBUG_VERBOSE)
#   define DLog(fmt, ...) NSLog((@"\n\n%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#   define DLog(...)
#endif


//----------------------------------------------

#pragma mark  DFP Step 1

#import <GoogleMobileAds/GoogleMobileAds.h>
#import "LAAdTags.h"

@interface LABannerView () 


- (void)configureBannerForView:(UIView *)aView;

@end

@implementation LABannerView

#ifdef DFPSAMPLE
// NOTE : just for test remove in production code snippet
@synthesize parentViewController;
#endif

#pragma mark  DFP Step 2

@synthesize adBanner = adBanner_;
@synthesize adTagsID;
@synthesize containerViewController;
@synthesize bckView;

@synthesize delegate;
@synthesize request;


#pragma mark - Initialization Methods

- (id)initWithContainerViewController:(UIViewController*)aContainerViewController AndAdTags:(LAAdTags*)aTagsList
{
    self = [self initWithContainerViewController:aContainerViewController AndAdTags:aTagsList AndAdditionalParameters:nil];
    

    
    return self;
}

- (id)initWithContainerViewController:(UIViewController*)aContainerViewController AndAdTags:(LAAdTags*)aTagsList enableLocation:(BOOL)enable
{
    self = [self initWithContainerViewController:aContainerViewController AndAdTags:aTagsList];
    

    
    return self;
}

// New instanciation implementation in order to add some extra info when requesting some ads banner
- (id)initWithContainerViewController:(UIViewController*)aContainerViewController AndAdTags:(LAAdTags*)aTagsList AndAdditionalParameters:(NSDictionary *)extrasParams
{
    self = [super init];
    if (self)
    {
        self.request = [self createRequest];
        
        // Define Extra params for keywords as example
        GADExtras *extras = [[GADExtras alloc] init];
        extras.additionalParameters = [NSMutableDictionary dictionaryWithDictionary:extrasParams];
        
        //====================================================================================================
        /*
         Paramètre destiné aux mineurs : Children's Online Privacy Protection Act (COPPA) 
         doc DFP : https://developers.google.com/mobile-ads-sdk/docs/admob/additional-controls#ios-coppa
         
         Si vous définissez tag_for_child_directed_treatment sur 1, vous indiquez que vous souhaitez voir votre contenu traité comme étant destiné aux mineurs conformément aux dispositions de la loi COPPA.
         Si vous définissez tag_for_child_directed_treatment sur 0, vous indiquez que vous ne souhaitez pas que votre contenu soit traité comme étant destiné aux enfants conformément aux dispositions de la loi COPPA.
         Si vous ne définissez pas tag_for_child_directed_treatment, les demandes d'annonces n'indiquent pas comment traiter votre contenu par rapport à la loi COPPA.
         */
        
        //[extras setValue:@"1" forKey:@"tag_for_child_directed_treatment"];
        
        //====================================================================================================
        
        [request registerAdNetworkExtras:extras];
        
        [self setAdTagsID:aTagsList];
        [self setContainerViewController:aContainerViewController];
    }
    
    return self;
}

- (id)initWithContainerViewController:(UIViewController*)aContainerViewController AndAdTags:(LAAdTags*)aTagsList AndAdditionalParameters:(NSDictionary *)extrasParams enableLocation:(BOOL)enable
{
    self = [self initWithContainerViewController:aContainerViewController AndAdTags:aTagsList AndAdditionalParameters:extrasParams];
    

    
    return self;
}

#pragma mark  DFP Step 6 - Banner Ad Display Methods

- (void)displayBannerAdOnView:(UIView*)aView
{
    [self configureBannerForView:aView];
    
    if (adBanner_ != nil)
    {
        // adBanner_ a generic request to load it with an ad.

        CLLocation * location = [[GeolocManager sharedInstance] getLastLocation];
        [request setLocationWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude accuracy:100];

        
        [adBanner_ loadRequest:request];
    }
}

- (void)displayBannerAdOnView:(UIView*)aView withLocation:(CLLocation *)location
{
    [self configureBannerForView:aView];
    
    if (adBanner_ != nil)
    {
        // adBanner_ a generic request to load it with an ad.
        
        if(location)
        {
            [request setLocationWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude accuracy:100];
        }
        
        [adBanner_ loadRequest:request];
    }
}

- (void)configureBannerForView:(UIView *)aView
{
    DLog(@" => displayBannerAdOnView: %@",[aView description]);
    
    // Define Ad Space Area
    if (adBanner_ != nil)
    {
        adBanner_.adSizeDelegate = nil;
        adBanner_.delegate = nil;
        [adBanner_ removeFromSuperview];
        adBanner_ = nil;
    }
    
    if (self.bckView != nil)
    {
        [bckView removeFromSuperview];
        bckView = nil;
    }
    
    GADAdSize size;
    NSMutableArray *validSizes = [NSMutableArray array];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) // iPAD CASE
    {
        if (UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation]))
        {
            size = GADAdSizeFromCGSize(CGSizeMake(768, 90));
            [validSizes addObject:[NSValue valueWithBytes:&size objCType:@encode(GADAdSize)]];
            UIView *tmpView = [[UIView alloc]initWithFrame:CGRectMake(0, aView.frame.size.height - (size.size.height), size.size.width, size.size.height)];
            [self setBckView:tmpView];
        }
        else
        {
            size = GADAdSizeFromCGSize(CGSizeMake(1024, 90));
            [validSizes addObject:[NSValue valueWithBytes:&size objCType:@encode(GADAdSize)]];
            UIView *tmpView = [[UIView alloc]initWithFrame:CGRectMake(0, aView.frame.size.height - (size.size.height), size.size.width, size.size.height)];
            
            [self setBckView:tmpView];
        }
        
        size = GADAdSizeFromCGSize(CGSizeMake(728, 90));
        [validSizes addObject:[NSValue valueWithBytes:&size objCType:@encode(GADAdSize)]];
        CGPoint origin = CGPointMake(0.0, aView.frame.size.height - size.size.height);
        adBanner_ = [[DFPBannerView alloc] initWithAdSize:size origin:origin];
    }
    else
    {
        size = GADAdSizeFromCGSize(CGSizeMake(320, 50));
        CGPoint origin = CGPointMake(0.0, aView.frame.size.height - size.size.height);
        adBanner_ = [[DFPBannerView alloc] initWithAdSize:kGADAdSizeBanner origin:origin];
        UIView *tmpView =[[UIView alloc]initWithFrame:CGRectMake(0, aView.frame.size.height - (size.size.height), [UIScreen mainScreen].applicationFrame.size.width, size.size.height)];
        
        [self setBckView:tmpView];
    }
    
    bckView.backgroundColor=[UIColor whiteColor];
    bckView.alpha = 0.0;
    [aView addSubview:bckView];
    
    adBanner_.validAdSizes = validSizes;
    adBanner_.delegate = self;
    adBanner_.alpha = 0.0;
    adBanner_.adSizeDelegate = self;
    adBanner_.rootViewController = containerViewController;
    [adBanner_ setAutoloadEnabled:false];

    [aView addSubview:adBanner_];

    
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation))
    {
        adBanner_.adUnitID = adTagsID.defaultPortraitTag;
       
    }
    else
    {
        adBanner_.adUnitID = adTagsID.defaultLandscapeTag;
    }
    
    DLog(@"=> Tag used : %@\n\n\n",adBanner_.adUnitID);

    
#ifdef DEBUG
    //adBanner_.layer.borderWidth = 2.0;
    //adBanner_.layer.borderColor = [UIColor purpleColor].CGColor;
#endif
}
- (void)displayAdSizeMediumRectangle:(UIView *)aView
{
    CGSize bannerSize = CGSizeZero;
    
    bannerSize = CGSizeFromGADAdSize(kGADAdSizeMediumRectangle);
    
    // Define Ad Space Area
    if (adBanner_ != nil)
    {
        adBanner_.adSizeDelegate = nil;
        adBanner_.delegate = nil;
        [adBanner_ removeFromSuperview];
        adBanner_ = nil;
    }
    
    CGPoint origin = CGPointMake(0.0, aView.frame.size.height - bannerSize.height);
    adBanner_ = [[DFPBannerView alloc] initWithAdSize:kGADAdSizeMediumRectangle origin:origin];
    adBanner_.delegate = self;
    
    adBanner_.alpha = 0.0;
    
    [aView addSubview:adBanner_];
    
    adBanner_.adSizeDelegate = self;
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    adBanner_.rootViewController = containerViewController;
    adBanner_.adUnitID = adTagsID.defaultLandscapeTag;
    
    if (adBanner_ != nil)
    {
        CLLocation * location = [[GeolocManager sharedInstance] getLastLocation];
        [request setLocationWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude accuracy:100];
        
        [adBanner_ loadRequest:request];
    }
}

// modif Ajout banner rectangulaire

- (void)displayNativeBanner:(UIView *)aView
{
    CGSize bannerSize = CGSizeZero;
    
    bannerSize = CGSizeFromGADAdSize(kGADAdSizeBanner);
    
    // Define Ad Space Area
    if (adBanner_ != nil)
    {
        adBanner_.adSizeDelegate = nil;
        adBanner_.delegate = nil;
        [adBanner_ removeFromSuperview];
         adBanner_ = nil;
    }
    
    CGPoint origin = CGPointMake(0.0, aView.frame.size.height - bannerSize.height);
    adBanner_ = [[DFPBannerView alloc] initWithAdSize:kGADAdSizeBanner origin:origin];
    adBanner_.delegate = self;
    
    adBanner_.alpha = 0.0;
    
    [aView addSubview:adBanner_];
    
    adBanner_.adSizeDelegate = self;
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    adBanner_.rootViewController = containerViewController;
    adBanner_.adUnitID = adTagsID.defaultLandscapeTag;
    
    if (adBanner_ != nil)
    {
        CLLocation * location = [[GeolocManager sharedInstance] getLastLocation];
        [request setLocationWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude accuracy:100];
        
        [adBanner_ loadRequest:request];
    }
}


#pragma mark - GADRequest generation

// Here we're creating a simple DFPRequest and whitelisting the application
// for test ads. You should request test ads during development to avoid
// generating invalid impressions and clicks.
- (DFPRequest *)createRequest
{
    DFPRequest * uriRequest = [DFPRequest request];
    
    // Make the request for a test ad. Put in an identifier for the simulator as
    // well as any devices you want to receive test ads.
#if TARGET_IPHONE_SIMULATOR
//    uriRequest.testDevices = @[ kDFPSimulatorID ];
#else
    uriRequest.testDevices = [NSArray arrayWithObjects:nil];
#endif
    
    return uriRequest;
}

#pragma mark - GADBannerViewDelegate implementation

// We've received an ad successfully.
- (void)adViewDidReceiveAd:(GADBannerView *)adView
{
    DLog(@" => Received ad successfully");
    
   // [getDelegate setCanRequestBannerAd:TRUE];
    
    //cas bug view taille à 0
    NSArray *bannerSubViews = [adBanner_ subviews];
    
    if([bannerSubViews count] >0)
    {
        UIView *moPubAds = [bannerSubViews objectAtIndex:0];
        CGRect adFrame = moPubAds.frame;
        
        if(adFrame.size.width ==0)
        {
            if( [moPubAds isKindOfClass:[MPAdView class]])
            {
                adFrame.size.width = 728;
                adFrame.size.height = 90;
                moPubAds.frame = adFrame;
            }
            
            long widthDiff = adView.superview.frame.size.width - 728;
            adFrame = adView.frame;
            adFrame.origin.x = adFrame.origin.x + (widthDiff/2.0);
            adView.frame = adFrame;
        }
    }
    
    // Center Ad if possible
    if (adView.frame.size.width < adView.superview.frame.size.width)
    {
        long widthDiff = adView.superview.frame.size.width - adView.frame.size.width;
        CGRect adFrame = adView.frame;
        adFrame.origin.x = adFrame.origin.x + (widthDiff/2.0);
        adView.frame = adFrame;
    }
    
    //Update banner alpha
    [UIView beginAnimations:@"showAd" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [adBanner_ setAlpha:1.0];
    if(bckView!=nil) [bckView setAlpha:1.0];
    [UIView commitAnimations];

    if ((delegate != nil) && [delegate respondsToSelector:@selector(needUpdateViewLayoutWithAdIsDisplay:forBannerView:)])
    {
        [delegate needUpdateViewLayoutWithAdIsDisplay:TRUE forBannerView:self];
    }
}

- (void)adView:(DFPBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    DLog(@" => Failed to receive ad with error: %@", [error localizedFailureReason]);
    
   //[getDelegate setCanRequestBannerAd:TRUE];
    
    //Update banner alpha
    [UIView beginAnimations:@"hideAd" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [adBanner_ setAlpha:0.0];
    [bckView setAlpha:0.0];

    [UIView commitAnimations];
    
    // Make other transformations on content view
    if ((delegate != nil) && [delegate respondsToSelector:@selector(needUpdateViewLayoutWithAdIsDisplay:forBannerView:)])
    {
        [delegate needUpdateViewLayoutWithAdIsDisplay:FALSE forBannerView:self];
    }
    
#ifdef DFPSAMPLE
#warning NOTE : just for test remove in production code snippet
    if ([parentViewController respondsToSelector:@selector(showError:)])
    {
        [parentViewController showError:error];
    }
#endif
}

#pragma mark - Click-Time Lifecycle Notifications

// Sent just before presenting the user a full screen view, such as a browser,
// in response to clicking on an ad.  Use this opportunity to stop animations,
// time sensitive interactions, etc.
//
// Normally the user looks at the ad, dismisses it, and control returns to your
// application by calling adViewDidDismissScreen:.  However if the user hits the
// Home button or clicks on an App Store link your application will end.  On iOS
// 4.0+ the next method called will be applicationWillResignActive: of your
// UIViewController (UIApplicationWillResignActiveNotification).  Immediately
// after that adViewWillLeaveApplication: is called.
- (void)adViewWillPresentScreen:(DFPBannerView *)adView
{
    DLog(@" => adViewWillPresentScreen");
}

// Sent just before dismissing a full screen view.
- (void)adViewWillDismissScreen:(DFPBannerView *)adView
{
    DLog(@"=> adViewWillDismissScreen");
    
    //Update banner alpha
    [UIView beginAnimations:@"hideAd" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [adBanner_ setAlpha:0.0];
    if(bckView!=nil) [bckView setAlpha:0.0];

    [UIView commitAnimations];
    
    // Make other transformations on content view
    if ((delegate != nil) && [delegate respondsToSelector:@selector(needUpdateViewLayoutWithAdIsDisplay:forBannerView:)])
    {
        [delegate needUpdateViewLayoutWithAdIsDisplay:FALSE forBannerView:self];
    }
}

// Sent just after dismissing a full screen view.  Use this opportunity to
// restart anything you may have stopped as part of adViewWillPresentScreen:.
- (void)adViewDidDismissScreen:(DFPBannerView *)adView
{
    DLog(@" => adViewDidDismissScreen");
}


// Sent just before the application will background or terminate because the
// user clicked on an ad that will launch another application (such as the App
// Store).  The normal UIApplicationDelegate methods, like
// applicationDidEnterBackground:, will be called immediately before this.
- (void)adViewWillLeaveApplication:(DFPBannerView *)adView
{
    DLog(@" => adViewWillLeaveApplication");
    //Update banner alpha
    [UIView beginAnimations:@"hideAd" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [adBanner_ setAlpha:0.0];
    if(bckView!=nil) [bckView setAlpha:0.0];
    [UIView commitAnimations];
    
    // Make other transformations on content view
    if ((delegate != nil) && [delegate respondsToSelector:@selector(needUpdateViewLayoutWithAdIsDisplay:forBannerView:)])
    {
        [delegate needUpdateViewLayoutWithAdIsDisplay:FALSE forBannerView:self];
    }
}

#pragma  mark - GADAdSizeDelegate Method

- (void)adView:(DFPBannerView *)view willChangeAdSizeTo:(GADAdSize)size
{
    DLog(@"=> willChangeAdSizeTo : %@\n\n",NSStringFromCGSize(size.size));
}

//- (UIViewController *)viewControllerForPresentingModalView
//{
//    if ([getDelegate window].rootViewController.presentedViewController == nil)
//    {
//        return [getDelegate window].rootViewController;
//    }
//    else
//    {
//        return [getDelegate window].rootViewController.presentedViewController;
//    }
//}


@end
