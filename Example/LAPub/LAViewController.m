//
//  LAViewController.m
//  LAPub
//
//  Created by Stéphane Couzinier on 11/04/2015.
//  Copyright (c) 2015 Stéphane Couzinier. All rights reserved.
//

#import "LAViewController.h"
#import "LAAdTags.h"
#import "LABannerView.h"
@interface LAViewController ()

@end

@implementation LAViewController
@synthesize bannerView; // DFP

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
    [self requestBannerAdvertisement];

}

- (void)requestBannerAdvertisement
{
    
        // DFP
        @try
        {
            [self destroyAdvertBanner];
            
            LAAdTags *bannerTags;
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            {
                bannerTags = [[LAAdTags alloc] initWithTag:@"/12271007/dfp_mobiles_tele7jours_s134734/banniere_rg_r167607/f167610_d205_dm3_bas_centre_ban_320x50"];
            }
            else
            {
                bannerTags = [[LAAdTags alloc] initWithPortraitTag:@"/12271007/dfp_mobiles_tele7jours_s134734/banniere_rg_r167607/f167610_d205_dm3_bas_centre_ban_320x50" AndLandscapeTag:@"/12271007/dfp_mobiles_tele7jours_s134734/banniere_rg_r167607/f167610_d205_dm3_bas_centre_ban_320x50"];
            }
            
            LABannerView *tmp = [[LABannerView alloc] initWithContainerViewController:self AndAdTags:bannerTags enableLocation:YES];
            [self setBannerView:tmp];

            
            self.bannerView.delegate = self;
            [self.bannerView displayBannerAdOnView:self.view];
            [self.bannerView.adBanner setTransform:CGAffineTransformMakeTranslation(0, 50)];

        }
        @catch (NSException *exception)
        {
            NSLog(@"\n\n\n!!! ScrollNewsViewController : DisplayBannerAd raise an exception => name : %@ | reason : %@ !!! \n\n",[exception name],[exception reason]);
        }
}
- (void)needUpdateViewLayoutWithAdIsDisplay:(BOOL)adIsDisplay forBannerView:(LABannerView *)theBanner
{
    if (adIsDisplay == YES)
    {
        // Banner advert is display need update content view
        //Display Animation
        CGContextRef context = UIGraphicsGetCurrentContext();
        [UIView beginAnimations:nil context:context];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:0.4];
        [self.bannerView.adBanner setTransform:CGAffineTransformMakeTranslation(0, 0)];
        [UIView commitAnimations];// Complete the animation
    }
    else
    {
        [self.bannerView.bckView setAlpha:0.0];
        // Banner advert is hidden need update content view
        [self.bannerView.adBanner setTransform:CGAffineTransformMakeTranslation(0, theBanner.adBanner.frame.size.height)];
    }
}
- (void)destroyAdvertBanner
{
    [self needUpdateViewLayoutWithAdIsDisplay:FALSE forBannerView:self.bannerView];
    
    if (self.bannerView != nil)
    {
        [self.bannerView.adBanner removeFromSuperview];
        
        [self.bannerView setContainerViewController:nil];
        [self.bannerView setDelegate:nil];
        bannerView = nil;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
