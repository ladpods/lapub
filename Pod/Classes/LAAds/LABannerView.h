//
//  LABannerView.h
//  LAD-DFP
//
//  Created by DUPUY Yann on 21/12/12.
//  Copyright (c) 2012 DUPUY Yann. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreLocation/CoreLocation.h>

//DFP
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "MPAdView.h"

@class LABannerView;

@protocol LABannerViewDelegate <NSObject>

@optional

/// Inform container UIViewController need adapt his layout accoding ad display state
- (void)needUpdateViewLayoutWithAdIsDisplay:(BOOL)adIsDisplay forBannerView:(LABannerView *)theBanner;

@end

@class  DFPRequest;
@class LAAdTags;

@interface LABannerView : NSObject <GADBannerViewDelegate,GADAdSizeDelegate>
{
    // DFP
    DFPBannerView               *adBanner_;
    LAAdTags                    *adTagsID;
    UIViewController            *containerViewController;
    id<LABannerViewDelegate>    delegate;
    DFPRequest                  *request;
    UIView                      *bckView;

    
#ifdef DFPSAMPLE
// NOTE : just for test remove in production code snippet
    UIViewController            *parentViewController;
#endif
}

/************* ATTRIBUTES *************/

// DFP
@property (nonatomic, strong)               DFPBannerView               *adBanner;
@property (nonatomic, strong)               UIView                      *bckView;
@property (nonatomic, strong)               LAAdTags                    *adTagsID;
@property (nonatomic)                 UIViewController            *containerViewController;
@property (nonatomic)                 id<LABannerViewDelegate>	delegate;
@property (nonatomic, strong)               DFPRequest                  *request;

#ifdef DFPSAMPLE
// NOTE : just for test remove in production code snippet
@property (nonatomic, strong)               UIViewController        *parentViewController;
#endif

/************** METHODS **************/

/// Initialization for banner ad space
- (id)initWithContainerViewController:(UIViewController*)aContainerViewController AndAdTags:(LAAdTags*)aTagsList;
- (id)initWithContainerViewController:(UIViewController*)aContainerViewController AndAdTags:(LAAdTags*)aTagsList enableLocation:(BOOL)enable;
- (id)initWithContainerViewController:(UIViewController*)aContainerViewController AndAdTags:(LAAdTags*)aTagsList AndAdditionalParameters:(NSDictionary *)extrasParams;
- (id)initWithContainerViewController:(UIViewController*)aContainerViewController AndAdTags:(LAAdTags*)aTagsList AndAdditionalParameters:(NSDictionary *)extrasParams enableLocation:(BOOL)enable;

/// Ask to display banner on area aView;
- (void)displayBannerAdOnView:(UIView*)aView;
- (void)displayBannerAdOnView:(UIView*)aView withLocation:(CLLocation *)location;
- (void)displayAdSizeMediumRectangle:(UIView *)aView;
- (void)displayNativeBanner:(UIView *)aView;
/**
 *  @author yann dupuy, 15-03-23 19:03:03
 *
 *  @brief  Create a DFP request for get advertisement configuration
 *
 *  @return DFP ad request object
 */
- (DFPRequest *)createRequest;

@end
