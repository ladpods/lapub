//
//  LAPartenaire.m
//  Elle
//
//  Created by COUZINIER Stephane on 08/09/2015.
//  Copyright (c) 2015 TEIXEIRA Yohan. All rights reserved.
//  Updated by Laurine Baillet on 17/01/2018
//

#import "LAPartenaire.h"

// Network
#import <AFNetworking/AFHTTPSessionManager.h>

// ---------------------------------------------------------------------------------
#pragma mark - Interface
// ---------------------------------------------------------------------------------

@interface LAPartenaire (/* Private */)


@property (nonatomic, strong)   AFHTTPSessionManager *  downloadSessionManager;

@end

// ---------------------------------------------------------------------------------
#pragma mark - Implementation
// ---------------------------------------------------------------------------------

@implementation LAPartenaire

// ---------------------------------------------------------------------------------
#pragma mark - Initialization
// ---------------------------------------------------------------------------------

- (id)init
{
    if (self = [super init])
    {
        
    }
    return self;
}

// ---------------------------------------------------------------------------------
#pragma mark - Getter
// ---------------------------------------------------------------------------------

- (AFHTTPSessionManager *)downloadSessionManager {
    if (!_downloadSessionManager) {
        _downloadSessionManager = [AFHTTPSessionManager manager];
        _downloadSessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }
    
    return _downloadSessionManager;
}

// ---------------------------------------------------------------------------------
#pragma mark - Public
// ---------------------------------------------------------------------------------
/*!
 @brief it save a file with his name, if server is not available, risk of no template
 
 @param  url The url of the file on the server
 
 @param withFileName the file name with the extension
 
 */
- (void)saveUrl:(NSString *)url toFile:(NSString *)fileName completion:(void (^)(void))completion
{
    if (!url) {
        return;
    }
    
    // Create directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"remote"];
    
    NSError *error;
    if(![[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:path
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:&error];
        if(error!=nil)
            return;
    }
    
    
    NSURL *fileDirectoryPath = [NSURL fileURLWithPath:path];
    
    // Download the file
    NSURL *fileURL = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:fileURL];
    
    NSURLSessionDownloadTask *downloadTask = [self.downloadSessionManager downloadTaskWithRequest:request progress:nil destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        return [fileDirectoryPath URLByAppendingPathComponent:fileName];
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        completion();
    }];
    
    [downloadTask resume];
}
// ---------------------------------------------------------------------------------
/*!
 @brief it save a file with his name, if server is not available, the default template will be use.
 
 @param url The url of the file on the server
 
 @param fileName the file name with the extension
 
 @param valuesToCheck An array of NSString that has to be in the template saved (check of the template return by the server). Can be nil or empty
 
 */
- (void)downloadRessourceAtUrl:(NSString *)url withFileName:(NSString *)fileName andValuesToCheck:(NSArray*)valuesToCheck completion:(void (^)(void))completion
{
    if (!url || !fileName) {
        return;
    }
    NSString* webStringURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    // Create directories
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"remote"];
    [self createDirectoryAtPath:path];
    
    //Create urls
    NSURL *fileDirectoryPath = [NSURL fileURLWithPath:path];
    NSURL *fileURL = [NSURL URLWithString:webStringURL];
    
    //#ifdef DEBUG
    ////    Test with any server response
    //    fileURL = [NSURL URLWithString:@"https://httpstat.us/503"];
    //#endif
    
    NSURLRequest *request = [NSURLRequest requestWithURL:fileURL];
    
    // Download the file
    NSURLSessionDownloadTask *downloadTask = [self.downloadSessionManager downloadTaskWithRequest:request progress:nil destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        
        NSURL* destination = [fileDirectoryPath URLByAppendingPathComponent:fileName];
        [[NSFileManager defaultManager] removeItemAtURL:destination error:nil];
        NSLog(@"LA Pub : download template with name : %@",fileName);
        return destination;
        
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        
        NSURL* destination = [fileDirectoryPath URLByAppendingPathComponent:fileName];
        
        if(httpResponse.statusCode == 200) {
            //In completion block if not : move cannot be executed on a file not writen yet
            if([self checkValues:valuesToCheck inTemplateAtPath:destination]) {
            } else {
                NSLog(@"LA Pub Error : le template téléchargé %@ ne correspond pas à celui demandé. Vérifiez si le fichier mis en ligne n'a pas été modifié.", fileName);
                [self renameErrorFileWithName:fileName];
            }
        } else {
            [self renameErrorFileWithName:fileName];
            NSLog(@"LA Pub response status code: %ld, local template will be used.", (long)httpResponse.statusCode);
        }
        completion();
    }];
    
    [downloadTask resume];
}
// ---------------------------------------------------------------------------------
/*!
 @brief get the content of a file (html,json...) completed with parameters (local or remote)
 
 @param fileName The name of the file requested with the extension
 
 @param parameters to replace occurences of string in html. Key = occurence to replace (_BODY_), value = the value for the occurence (<body>bla bla</body>)
 
 */
- (NSString *)getContentForFile:(NSString *)fileName withParameter:(NSDictionary *)parameters
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"remote"];
    NSString *content = @"";
    NSStringEncoding encoding;
    
    NSString *filePath = [path stringByAppendingPathComponent:fileName];
    
    //On vérifie s'il a été renommé = si il contenait une erreur et qu'il faut utiliser le template local
    if(![[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        // On prend le template local
        NSString* extension = [fileName pathExtension];
        NSString* localFileName = [fileName stringByDeletingPathExtension];
        
        if ([[NSBundle mainBundle] pathForResource:localFileName ofType:extension] != nil) {
            filePath = [[NSBundle mainBundle] pathForResource:localFileName ofType:extension];
        }
    }
    
    //Si on a un fichier provenant du serveur ou un fichier local
    if(filePath != nil) {
        content = [NSString stringWithContentsOfFile:filePath  usedEncoding:&encoding  error:NULL];
    } else {
        NSLog(@"LA Pub error : there is no local or remote template available");
    }
    
    // On remplace les occurences pour les templates
    for (NSString* key in parameters) {
        content=[content stringByReplacingOccurrencesOfString:key withString:[parameters valueForKey:key]];
    }
    
    return content;
}
// ---------------------------------------------------------------------------------
#pragma mark - FileManager private methods
// ---------------------------------------------------------------------------------
/*!
 @brief Create a directory with [NSFileManager defaultManager]
 
 @param path the path of the directory to create
 
 */
- (void)createDirectoryAtPath:(NSString*)path
{
    NSError* error;
    if(![[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:path
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:&error];
        if(error!=nil) {
            NSLog(@"LA Pub error when create directory at path : %@ . Error description : %@",path,error.description);
        }
    }
}
// ---------------------------------------------------------------------------------
/*!
 @brief rename a file when the file is not valid
 
 @param fileName Original name of the file not valid
 */
- (void)renameErrorFileWithName:(NSString*)fileName
{
    if(fileName == nil)
        return;
    
    NSError* error;
    NSFileManager* fileManager = [NSFileManager defaultManager];
    
    //Make error path
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)
    ;
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"remote"];
    NSString* remoteFilePath =[path stringByAppendingPathComponent:fileName];
    
    //Create new file path with prefix error_ (to keep a track of the error)
    NSString* errorFileName = @"error_";
    errorFileName = [errorFileName stringByAppendingString:fileName];
    NSString *errorFilePath = [path stringByAppendingPathComponent:errorFileName];
    
    if([fileManager fileExistsAtPath:remoteFilePath]) {
        
        //S'il y a déjà un fichier d'erreur du meme nom, on le supprime pour ne pas avoir d'erreur lors du moveItemAtPath
        if([fileManager fileExistsAtPath:errorFilePath]) {
            
            [fileManager removeItemAtPath:errorFilePath error:&error];
            if(error != nil) {
                NSLog(@"LA Pub : error when removeItemAtPath : %@, error description : %@", errorFilePath, error.description);
                error = nil;
            }
        }
        
        [fileManager moveItemAtPath:remoteFilePath toPath:errorFilePath error:&error];
        if(error != nil)
            NSLog(@"LA Pub : error when moveItemAtPath : %@ toPath : %@,error description : %@", remoteFilePath, errorFilePath, error.description);
    } else {
        NSLog(@"LA Pub : error there is no file downloaded at path : %@", remoteFilePath);
    }
    
}
// ---------------------------------------------------------------------------------
#pragma mark - Check private methods
// ---------------------------------------------------------------------------------
/*!
 @brief Check if the template contains values
 
 @param values NSArray of string that has to be contained in template
 
 @param path path url of the template to check
 
 */
- (BOOL)checkValues:(NSArray*)values inTemplateAtPath:(NSURL*)path
{
    // S'il n'y a pas de valeurs à vérifier, on part du principe que c'est bon.
    if(values == nil || ([values isKindOfClass:[NSArray class]] && values.count == 0))
        return true;
    
    if([[NSFileManager defaultManager] fileExistsAtPath:path.relativePath])
    {
        NSStringEncoding encoding;
        NSString *content = [NSString stringWithContentsOfFile:path.relativePath  usedEncoding:&encoding  error:NULL];
        
        for(NSString* value in values) {
            if(![content containsString:value]) {
                return false;
            }
        }
        return true;
    }
    return false;
}


@end


