# LAPub

[![CI Status](http://img.shields.io/travis/Stéphane Couzinier/LAPub.svg?style=flat)](https://travis-ci.org/Stéphane Couzinier/LAPub)
[![Version](https://img.shields.io/cocoapods/v/LAPub.svg?style=flat)](http://cocoapods.org/pods/LAPub)
[![License](https://img.shields.io/cocoapods/l/LAPub.svg?style=flat)](http://cocoapods.org/pods/LAPub)
[![Platform](https://img.shields.io/cocoapods/p/LAPub.svg?style=flat)](http://cocoapods.org/pods/LAPub)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LAPub is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "LAPub"
```

## Author

Stéphane Couzinier, stephane.couzinier@lagardere-active.com

## License

LAPub is available under the MIT license. See the LICENSE file for more info.
