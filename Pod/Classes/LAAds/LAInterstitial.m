//
//  LAInterstitial.m
//  LAD-DFP
//
//  Created by DUPUY Yann on 21/12/12.
//  Copyright (c) 2012 DUPUY Yann. All rights reserved.
//

#import "LAInterstitial.h"
#pragma mark  DFP Step 1
#import "LAAdTags.h"

//#import "AppDelegate.h"
#import "GeolocManager.h"
//#import "ObjectTools.h"

//---------------- DEBUG MACRO ------------------

#pragma mark - Debug configuration

//#define DEBUG_ENABLE // Activate Debug log only in this file

/**
 *  - Activate Debug Log for all file  if  "DEBUG_VERBOSE" define in GCC PREPROCESSOR MACRO (build setting)
 *  - Activate Debug only in a file if variable DEBUG_ENABLE  define in the file
 */

#if (defined DEBUG_ENABLE || defined DEBUG_VERBOSE)
#   define DLog(fmt, ...) NSLog((@"\n\n%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#   define DLog(...)
#endif


//----------------------------------------------

@interface LAInterstitial ()

-(DFPRequest *)createRequest;
@end

@implementation LAInterstitial

@synthesize interstitial = interstitial_;
@synthesize containerViewController = containerViewController_;
@synthesize adTagsID;
@synthesize splashImage;
@synthesize interIsOnScreen;

#pragma mark - Memory Methods



#pragma mark - Initialization Methods

/// Init Interstitial Ad for iPhone (Only for one interface Orientation )
- (id)initWithAndParentContainer:(UIViewController*)aParentViewController AndAdTagID:(LAAdTags*)aTagsID

{
    self = [super init];
    
    if (self)
    {
#pragma mark  DFP Step 2
        // DFP - Prepare Inter Ad
        [self setContainerViewController:aParentViewController];
        [self setAdTagsID:aTagsID];
        interIsOnScreen = FALSE;
        
    }
    
    return self;
}

// Init Interstitial Ad for splash on app Startup
- (id)initForSplashAdWithAdtagID:(LAAdTags*)aTagsID
{
    self = [super init];
    
    if (self)
    {
        [self setAdTagsID:aTagsID];
        interIsOnScreen = FALSE;
    }
    
    return self;
}

#pragma mark - Ad display Methods

/// Make an Interstitial display on a normal viewControler
- (void)displayInterAd
{
    DLog(@" => displayInterAd\n\n");
    
    if (interstitial_ != nil)
    {
        interstitial_.delegate = nil;
        interstitial_ = nil;
    }
    
    // Configure good Tag unit depending on device and orientation
    if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation))
    {
        interstitial_ = [[DFPInterstitial alloc] initWithAdUnitID:adTagsID.defaultPortraitTag];
    }
    else
    {
        interstitial_ = [[DFPInterstitial alloc] initWithAdUnitID:adTagsID.defaultLandscapeTag];
    }
    [interstitial_ setDelegate:self];
    
    DLog(@"=> ask display with TagID : %@\n\n",[interstitial_ adUnitID]);
    
    // make ad request
    [interstitial_ loadRequest:[GADRequest request]];
}

// implementation v2.0

- (void)displaySplashAdOnViewController: (UIViewController*)currentViewController AndWithSplashImg:(BOOL)activateSplash
{
    DLog(@" => displaySplashAdOnViewController\n\n");
    
    if (interstitial_ != nil) // clean up any old DFPInterstitial Object
    {
        interstitial_.delegate = nil;
        interstitial_=nil;
    }
    
    //Prepare ad Space
    
    [self setContainerViewController:currentViewController];
    
    // Configure good Tag unit depending on device and orientation
    if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation))
    {
        interstitial_ = [[DFPInterstitial alloc] initWithAdUnitID:adTagsID.defaultPortraitTag];
    }
    else
    {
        interstitial_ = [[DFPInterstitial alloc] initWithAdUnitID:adTagsID.defaultLandscapeTag];
    }
    [interstitial_ setDelegate:self];
    
    DLog(@" => interstitial_.adUnitID  Selected:%@\n\n\n",interstitial_.adUnitID);
    
    // make ad request
    // [interstitial_ loadRequest:[GADRequest request]];
    // make ad request
    DFPRequest * request = [self createRequest];
    CLLocation * location = [[GeolocManager sharedInstance] getLastLocation];
    [request setLocationWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude accuracy:100];
    
    //request.testDevices = @[ kDFPSimulatorID ];
    [interstitial_ loadRequest:request];
    
    if (activateSplash== TRUE)
    {
        //     [self displaySplashImage]; // Change app rootview controller to splash view controller which manage DFP Interstitial and Default image display
    }
}

- (void)displaySplashImage
{
    DLog(@" => displaySplashImage\n\n");
    
    UIImage *image = nil;
    
    //Define Default Splashscreen image to display
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        image = [UIImage imageNamed:@"Default"];
    }
    else
    {
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            image = [UIImage imageNamed:@"Default-Landscape"];
        }
        else
        {
            image = [UIImage imageNamed:@"Default-Portrait"];
        }
        
        if (nil == image)
        {
            image = [UIImage imageNamed:@"Default"];
        }
    }
    
    UIView *rootView = nil;
    
    if ([containerViewController_ isKindOfClass:[UIViewController class]])
    {
        rootView = self.containerViewController.view;
    }
    else
    {
        rootView = self.containerViewController.navigationController.navigationBar;
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    [self setSplashImage:imageView];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGRect frame = imageView.frame;
        frame.origin.y = -20;
        imageView.frame = frame;
    }
    
    [rootView addSubview:splashImage];
}

- (void)removeSplashImage
{
    DLog(@" => removeSplashImage\n\n");
    
    if (splashImage != nil)// assure Splash Image is remove from containerViewController
    {
        [splashImage removeFromSuperview];
        splashImage = nil;
    }
}

#pragma mark - Interstitial Delegate Methods

- (void)interstitialDidReceiveAd:(DFPInterstitial *)interstitial
{
    DLog(@" => interstitialDidReceiveAd\n\n");
    interIsOnScreen = TRUE;
    [interstitial_ presentFromRootViewController:containerViewController_];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dfpInterIsDisplay" object:nil]; // force Capptain Notif display if one was hide during inter display
}

- (void)interstitial:(DFPInterstitial *)interstitial didFailToReceiveAdWithError:(GADRequestError *)error
{
    DLog(@" => didFailToReceiveAdWithError :%@\n\n\n",[error description]);
    if (splashImage != nil)
    {
        [self removeSplashImage];
    }
}

- (void)interstitialWillPresentScreen:(DFPInterstitial *)interstitial
{
    DLog(@" => interstitialWillPresentScreen\n\n");
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dfpInterIsDisplay" object:nil];// force Capptain Notif hidden if one was hide during inter display
    interIsOnScreen = TRUE;
}

- (void)interstitialDidDismissScreen:(DFPInterstitial *)interstitial
{
    DLog(@" => interstitialDidDismissScreen\n\n");
    interIsOnScreen = FALSE;
    
    if (splashImage != nil) // Splash Case
    {
        [self removeSplashImage];
    }
    [self.delegate didDismissInterstitial];
}

- (void)interstitialWillDismissScreen:(DFPInterstitial *)interstitial
{
    DLog(@" => interstitialWillDismissScreen\n\n");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dfpInterIsDismiss" object:nil]; // force Capptain Notif display if one was hide during inter display
    interIsOnScreen = FALSE;
    
    if (splashImage != nil)// Splash Case
    {
        [self removeSplashImage];
    }
}

- (void)interstitialWillLeaveApplication:(DFPInterstitial *)interstitial
{
    DLog(@" => interstitialWillLeaveApplication\n\n");
}
-(DFPRequest *)createRequest
{
    DFPRequest * request = [[DFPRequest alloc] init];
    
    return request;
}

@end
