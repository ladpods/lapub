//
//  LAInterstitial.h
//  LAD-DFP
//
//  Created by DUPUY Yann on 21/12/12.
//  Copyright (c) 2012 DUPUY Yann. All rights reserved.
//

#import <UIKit/UIKit.h>

//@import GoogleMobileAds;
#import <GoogleMobileAds/DFPInterstitial.h>
#import <GoogleMobileAds/DFPRequest.h>

@class LAAdTags;


@protocol LAInterstitialDelegate;
@interface LAInterstitial : UIView <GADInterstitialDelegate>
{
    DFPInterstitial     *interstitial_;
    UIViewController    *containerViewController_;
    LAAdTags            *adTagsID;
    BOOL                interIsOnScreen;
}

/************* ATTRIBUTES *************/

@property (nonatomic, strong)   DFPInterstitial         *interstitial;
@property (nonatomic)     UIViewController        *containerViewController;
@property (nonatomic, strong)   LAAdTags                *adTagsID;
@property (nonatomic, strong)   UIImageView             *splashImage;
@property (nonatomic, assign)   BOOL                    interIsOnScreen;
@property (nonatomic, strong)   id<LAInterstitialDelegate> delegate;

/************** Methods **************/

#pragma mark - Initialization Methods

/// Init Interstitial Ad for iPhone (Only for one interface Orientation )
- (id)initWithAndParentContainer:(UIViewController*)aParentViewController AndAdTagID:(LAAdTags*)aTagsID;

// Init Interstitial Ad for splash on app Startup
- (id)initForSplashAdWithAdtagID:(LAAdTags*)aTagsID;

#pragma mark - Ad display Methods

/// Make an Interstitial displayon a normal viewControler
- (void)displayInterAd;

/// Make an Interstitial display on window after app startup
- (void)displaySplashAdOnViewController: (UIViewController*)currentViewController AndWithSplashImg:(BOOL)activateSplash;

- (void)removeSplashImage;

@end


@protocol LAInterstitialDelegate <NSObject>
- (void)didDismissInterstitial;
@end
