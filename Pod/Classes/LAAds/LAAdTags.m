//
//  LAAdTags.m
//  LAD-DFP
//
//  Created by DUPUY Yann on 26/12/12.
//  Copyright (c) 2012 DUPUY Yann. All rights reserved.
//

#import "LAAdTags.h"

@implementation LAAdTags

@synthesize defaultPortraitTag;
@synthesize defaultLandscapeTag;

#pragma mark - Initialization Methods

/// Default Initialization
- (id)initWithTag:(NSString*) aTag
{
    self = [super init];
    
    if (self)
    {
        [self setDefaultPortraitTag:aTag];
        [self setDefaultLandscapeTag:aTag];
    }
    
    return  self;
}

/// Default initialization with take in account of orientation (Portrait/Landscape)
- (id)initWithPortraitTag:(NSString*) aPortraitTag AndLandscapeTag:(NSString*) aLandscapeTag
{
    self = [super init];
    
    if (self)
    {
        [self setDefaultPortraitTag:aPortraitTag];
        [self setDefaultLandscapeTag:aLandscapeTag];
    }
    
    return  self;
}



#pragma mark - Convenient Methods

/// Define if device is an iPhone 5 model
- (BOOL)isiPhone5
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) //case iPhone
    {
        if ([UIScreen mainScreen].bounds.size.height > 480)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    else
    {
        return FALSE;
    }
}

/// Define if device as a retina screen
- (BOOL)isRetina
{
    if ([UIScreen mainScreen].scale > 1.0)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

@end
