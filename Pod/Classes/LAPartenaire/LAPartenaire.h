//
//  LAPartenaire.h
//  Elle
//
//  Created by COUZINIER Stephane on 08/09/2015.
//  Copyright (c) 2015 TEIXEIRA Yohan. All rights reserved.
//  Updated by Laurine Baillet on 17/01/2018
//

#ifndef Elle_LAPartenaire_h
#define Elle_LAPartenaire_h


#endif
#import <Foundation/Foundation.h>

//#import "TAGContainer.h"
// ---------------------------------------------------------------------------------
@interface LAPartenaire : NSObject
{
    
}
// ---------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------
/**
 *  @brief Save an URL content.
 *
 *  @param url        The URL string.
 *  @param fileName   The filename string.
 *  @param completion The completion executed when the download finishes.
 */

-(void) saveUrl:(NSString *)url toFile:(NSString *)fileName completion:(void (^) (void))completion __deprecated_msg("Use downloadRessourceAtUrl:withFileName:andValuesToCheck instead to provide bad template donwload");

// ---------------------------------------------------------------------------------
/*!
 @brief it save a file with his name, if server is not available, the default template will be use.
 
 @param  url The url of the file on the server
 
 @param withFileName the file name with the extension
 
 @param andValuesToCheck An array of NSString that has to be in the template saved (check of the template return by the server). Can be nil or empty
 
 */
- (void)downloadRessourceAtUrl:(NSString *)url withFileName:(NSString *)fileName andValuesToCheck:(NSArray*)valuesToCheck completion:(void (^)(void))completion;

// ---------------------------------------------------------------------------------
/*!
 @brief get the content of a file (html,json...) completed with parameters (local or remote)
 
 @param fileName The name of the file requested with the extension
 
 @param parameters to replace occurences of string in html. Key = occurence to replace (_BODY_), value = the value for the occurence (<body>bla bla</body>)
 
 */
- (NSString *)getContentForFile:(NSString *)fileName withParameter:(NSDictionary *)parameters;

@end


